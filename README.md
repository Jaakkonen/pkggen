# pkggen
Create Arch `PKGBUILD`s easily by just providing a URL.

## Running
```sh
$ pkggen <package name / link to repo / link to Pypi or NPM>
# Maintainer: Your name <mail@example.com>
pkgname=package
...
```

## Development
```sh
$ poetry install
$ poetry run pytest tests/
```
