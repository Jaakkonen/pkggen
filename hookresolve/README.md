# Hookresolve
Hookresolve is a small library allowing you to write dataclasses
that try to _resolve_ missing class attributes based on
resolver functions.

Example:
```py
from dataclasses import dataclass
import requests
from hookresolve import hookresolve


@hookresolve
@dataclass
class GithubRepo:
    organization: str
    name: str
    url: str
    stars: str

    # Hooks can be static methods or normal methods.
    # static methods are of course safer as you cannot
    # depend on any other than the given fields.
    @staticmethod
    def url_by_name_and_organization(name, organization):
        return f"https://github.com/{organization}/{name}"

    # Normal methods work file too.
    # Returned values should be in same order as the field names
    # in method name. Or the return should be a dict
    def name_and_project_by_url(self, url):
        r = re.search(
            r"https://github\.com/(?P<organization>\w+)/(?P<name>\w+)", url
        )
        return r.groups()

    # TODO: Using `self` in normal method body
    # will produce a warning.
    def stars_by_name_and_organization(self, name, organization):
        return requests.get(
            f"https://api.github.com/repos/{organization}/{name}"
        ).json()["stargazers_count"]


repo = GithubRepo(organization='psf', name='requests')
print(repo)
assert repo == GithubRepo('psf', 'requests')
```

## How it works?
Hookresolve takes the fields passed to it and resolves missing fields required by `def __init__(self, ...)`.
This works by looking at the signature of `__init__` and then finding fields that can be resolved with current found values.

## Comparison to other ways for doing this
Just dataclasses:
```py
@dataclass
class GithubRepo:
  # TODO: Add implementation here
  ...
```
This is pretty tedious because you need to hook every `__setattr__` and manually see which fields are already set and which should be calculated.

Just using properties:
You cannot have 2-way dependencies with properties. To implement this you would need to set the known fields to the instance `__dict__` and make it override the class properties (it probably works this way. TODO to check this).
This approach makes it so that all fields need resolvers and needs figuring out which fields are already resolved or possible to resolve without infinite recursion.
Overall this approach makes it hard to reason about if a given set of starting arguments can be resolved to all required values. (This argument is somewhat valid for Hookresolve too.)

## Maintainers
Jaakko Sirén <jaakko.siren@pm.me>
