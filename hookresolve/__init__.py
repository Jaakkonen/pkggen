from collections import defaultdict
from dis import Instruction
from typing import Any, Callable, Type, TypeVar
import inspect
from itertools import zip_longest
import re

_T = TypeVar("_T")


class SkipResolver(Exception):
    """Causes `hookresolve` to skip the resolver and try next one if possible."""

    pass

class CouldNotResolve(Exception):
    """Indicates that no resolver can be called with current set of arguments
    or that all resolvers were skipped.
    """

TResolvers = tuple[frozenset[str], list[str], Callable]

def hookresolve(cls: Type[_T]) -> Type[_T]:
    original_init = cls.__init__
    orig_init_spec = inspect.getfullargspec(original_init)
    # No support for *args, **kwargs as we cannot determine what should be resolved for those values
    assert orig_init_spec.varargs is None
    assert orig_init_spec.varkw is None
    required_args = set(orig_init_spec.args + orig_init_spec.kwonlyargs)

    resolvers: list[TResolvers] = [
        (inputs, outputs, resolver)
        for name, resolver in inspect.getmembers(cls, predicate=inspect.isfunction)
        if (
            match := re.match(
                r"(?P<outputs>[a-z]+(_and_[a-z]+)*)_by_(?P<inputs>[a-z]+(_and_[a-z]+)*)",
                name,
            )
        )
        if (inputs := frozenset(match.group("inputs").split("_and_")))
        if (outputs := match.group("outputs").split("_and_"))
    ]

    resolvers_by_output: dict[str, list[TResolvers]] = defaultdict(list)
    for inputs, outputs, resolver in resolvers:
        # The `self` argument may not be mentioned in inputs so get the actual arguments using inspect
        real_inputs = frozenset(inspect.getfullargspec(resolver).args)
        assert inputs | {"self"} == real_inputs | {
            "self"
        }, f"{resolver.__name__} real arguments ({real_inputs}) do not match name."
        for output in outputs:
            resolvers_by_output[output].append((real_inputs, outputs, resolver))

    def hooked_init(self: _T, *_args, **_kwargs):
        # Create dict of argument names and values
        kwargs = (
            # Default valued arguments
            dict(zip(orig_init_spec.args[::-1], orig_init_spec.defaults or ()))
            | (orig_init_spec.kwonlydefaults or {})
            # Non-self arguments
            | dict(zip(orig_init_spec.args[1:], _args))
            | _kwargs
        )
        kwargs["self"] = self

        # Simple check if there are solvers to figure out missing values.
        if unresolvable := required_args - kwargs.keys() - resolvers_by_output.keys():
            raise ValueError(f"Cannot resolve {unresolvable}")

        def resolve_var(resolvers: list[TResolvers], kwargs: dict[str, Any]):
            for inputs, outputs, resolver in resolvers:
                if inputs <= kwargs.keys():
                    try:
                        res = resolver(**{i: kwargs[i] for i in inputs})
                        if not isinstance(res, (list, tuple)):
                            res = (res,)
                        if len(outputs) != len(res):
                            raise ValueError(f"Results unexpected size. {outputs=}, got {res=}")
                        return dict(zip(outputs, res))
                    except SkipResolver:
                        continue
            raise CouldNotResolve("No ")

        # Resolve missing arguments one by one.
        while missing_args := required_args - kwargs.keys():
            for missing in missing_args:
                try:
                    # TODO: add support for resolvers returning multiple values
                    kwargs |= resolve_var(resolvers_by_output[missing], kwargs)
                    break
                except CouldNotResolve:
                    pass
            else:
                raise ValueError(
                    f"{missing_args} could not be resolved using {kwargs.keys()}"
                )

        return original_init(**kwargs)

    cls.__init__ = hooked_init
    return cls
