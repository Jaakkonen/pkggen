from . import SkipResolver, hookresolve
from dataclasses import dataclass
import requests
import re
import pytest


def test_hookresolve_dataclass():
    @hookresolve
    @dataclass(repr=False)
    class GithubRepo:
        name: str
        organization: str
        url: str
        stars: str

        # Hooks can be static methods or normal methods.
        # static methods are of course safer as you cannot
        # depend on any other than the given fields.
        @staticmethod
        def url_by_name_and_organization(name, organization):
            return f"https://github.com/{organization}/{name}"

        # Normal methods work file too.
        # Returned values should be in same order as the field names
        # in method name. Or the return should be a dict
        def name_and_organization_by_url(self, url):
            r = re.search(
                r"https://github\.com/(?P<organization>\w+)/(?P<name>\w+)", url
            )
            return r.groups()

        # TODO: Using `self` in normal method body
        # will produce a warning.
        def stars_by_name_and_organization(self, name, organization):
            return requests.get(
                f"https://api.github.com/repos/{organization}/{name}"
            ).json()["stargazers_count"]

    repo1 = GithubRepo(organization="psf", name="requests")
    repo2 = GithubRepo("black", "psf")


def test_hookresolve_kwonly():
    @hookresolve
    class Foo:
        def __init__(self, a, *, b):
            self.a = a
            self.b = b

        def a_by_b(self, b):
            return "a by b"

    f1 = Foo(1, b=2)
    f2 = Foo(b=2)
    with pytest.raises(Exception) as ex:
        f3 = Foo(1)  # b is not resolvable by a.


def test_default_value():
    @hookresolve
    class Foo:
        def __init__(self, a, b=2):
            self.a = a
            self.b = b

        def a_by_b(self, b):
            return "meme"

    f1 = Foo()


def test_kwargs_not_ok():
    with pytest.raises(AssertionError):

        @hookresolve
        class Foo:
            def __init__(self, **kwargs):
                ...


def test_varargs_not_ok():
    with pytest.raises(AssertionError):

        @hookresolve
        class Foo:
            def __init__(self, *args):
                ...


def test_dataclass_defaults():
    @hookresolve
    @dataclass
    class Foo:
        a: int
        d: str
        b: str = ""
        c: int = 123

        def d_by_c(self, c):
            return str(c)

    f1 = Foo(1)


def test_kwarg_defaults():
    @hookresolve
    class Foo:
        def __init__(self, a, *, b=1):
            self.a = a
            self.b = b

        def a_by_b(self, b):
            return b + 2

    f1 = Foo(2)
    assert f1.a == 2
    assert f1.b == 1
    f2 = Foo()
    assert f2.a == 3
    assert f2.b == 1

def test_multiple_resolvers_for_output():
    @hookresolve
    @dataclass
    class Foo:
        a: int
        d: int
        b: int = 3
        c: int = 2
        def a_and_d_by_b(self, b):
            raise SkipResolver()
        def d_by_b(self, b):
            return 2
        def a_by_c(self, c):
            return 1
    Foo()

def test_resolver_yielding_multiple():
    @hookresolve
    @dataclass
    class Foo:
        a: int
        b: int
        c: int =1
        def a_and_b_by_c(self, c):
            return 2,3
    f1 = Foo()
    assert f1.c == 1
    assert f1.a == 2
    assert f1.b == 3