from dataclasses import dataclass
from pkggen.resolver import PackageInfo, PkgType
from .pkgbuild import Pkgbuild, make_node_package
from typing import Literal, Optional, get_args
import typer
from enum import Enum
from urllib.parse import urlparse
import requests

app = typer.Typer()


@app.command()
def new(
    name: str,
    type: Optional[PkgType] = None,
    url: Optional[str] = None,
    git: bool = False,  # Make -git package
    latest_tag: bool = False,  # Use latest tagged version in -git package
):
    info = dict(
        name=name,
        type=type,
        url=url,
        git=git,
        # latest_tag=latest_tag
    )
    pkg = PackageInfo(**{k: v for k, v in info if v is not None})

    # print(str(pkgbuild))


if __name__ == "__main__":
    typer.run(app())
