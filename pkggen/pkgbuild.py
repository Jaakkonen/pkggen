from dataclasses import dataclass, field
from .snippets import BuildFun, CheckFun, PackageFun, PkgverFun, PrepareFun
from subprocess import run
from textwrap import dedent
from typing import Optional
from .snippets import package_node_src, pkgver_git, build_node_src

git_config = (
    lambda conf: run(("git", "config", conf), capture_output=True)
    .stdout.decode("utf-8")
    .strip()
)


@dataclass(repr=False)
class Maintainer:
    name: str
    email: str

    def __str__(self):
        return f"# Maintainer: {self.name} <{self.email}>"

    @classmethod
    def from_git(cls):
        return cls(name=git_config("user.name"), email=git_config("user.email"))


@dataclass
class Pkgbuild:
    pkgname: str
    package: PackageFun

    maintainers: tuple[Maintainer, ...] = field(
        default_factory=lambda: (Maintainer.from_git(),)
    )

    pkgver: str = "0"
    pkgrel: int = 1
    arch: tuple[str, ...] = ("any",)
    url: Optional[str] = None
    license: Optional[tuple[str, ...]] = None

    pkgdesc: Optional[str] = None
    depends: tuple[str, ...] = ()
    makedepends: tuple[str, ...] = ()
    checkdepends: tuple[str, ...] = ()
    optdepends: tuple[str, ...] = ()
    source: tuple[str, ...] = ()

    pkgver_fun: Optional[PkgverFun] = None
    prepare: Optional[PrepareFun] = None
    build: Optional[BuildFun] = None
    check: Optional[CheckFun] = None

    def __str__(self):
        newline = "\n"
        dependencies = "".join(
            f"""{depname}=(
  {f'{newline}  '.join(f"'{dep}'" for dep in deplist)}
)
"""
            if (deplist := getattr(self, depname))
            else ""
            for depname in ("depends", "makedepends", "optdepends", "checkdepends")
        )
        return (
            f"""\
{newline.join(str(r) for r in self.maintainers)}

pkgname={self.pkgname}
pkgver={self.pkgver}
pkgrel={self.pkgrel}
{f"pkgdesc={self.pkgdesc}" if self.pkgdesc else ""}
arch=({' '.join(self.arch)})
url={self.url}
{dependencies}
{f'''source=(
  {f"{newline}  ".join(self.source)}
)''' if self.source else ""}
"""
            + (self.prepare or "")
            + (self.pkgver_fun or "")
            + (self.build or "")
            + (self.package)
            + (self.check or "")
        )

def make_node_package(
  name: str,
  url: str
) -> Pkgbuild:
  assert url.startswith("https://github.com/")
  return Pkgbuild(
    pkgname=name,
    url=url,
    source=(f"$pkgname::git+{url}",),
    depends=('nodejs',),
    package=package_node_src,
    pkgver_fun=pkgver_git,
    build=build_node_src
  )


