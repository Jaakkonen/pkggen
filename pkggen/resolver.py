from dataclasses import dataclass
from typing import Optional
import requests
from hookresolve import SkipResolver, hookresolve
from enum import Enum
from pkggen import vsc_gallery as vsc_client

PYPI_NAME = r"[a-zA-Z]"


class PkgType(str, Enum):
    NODE = "nodejs"
    PYTHON = "python"
    VSCODE = "vscode-extension"


# TODO: add resolvers using Tidelift's libraries.io service
# (not yet done as that requires API key)
@hookresolve
@dataclass
class PackageInfo:
    namehint: str  # Provided by user.

    url: str
    pkgtype: PkgType
    name: str  # Name without package type
    license: Optional[str]
    git: Optional[str]
    version: str

    def pkgtype_by_git(self, git):
        # TODO: Add support for github and gitlab apis here.
        # bitbucket, codeberg and sr.ht can be added at some point if there are APIs to do that.
        raise SkipResolver()

    def name_and_pkgtype_by_namehint(self, namehint: str) -> tuple[str, PkgType]:
        prefixes = {
            "python-": PkgType.PYTHON,
            "nodejs-": PkgType.NODE,
            "vscode-extension-": PkgType.VSCODE,
            # todo github url
        }
        for prefix, pkgtype in prefixes.items():
            if namehint.startswith(prefix):
                name = namehint.removeprefix(prefix)
                return name, pkgtype
        raise SkipResolver()

    def url_and_git_and_license_and_version_by_pkgtype_and_name(
        self, pkgtype: PkgType, name: str
    ):
        if pkgtype == PkgType.PYTHON:
            r = requests.get(f"https://pypi.org/pypi/{name}/json").json()
            source = r["project_urls"]["Source"]
            assert source.startswith("https://github.com") or source.startwith(
                "https://gitlab.com"
            )
            return r["info"], source, r["license"], r["version"]
        if pkgtype == PkgType.NODE:
            # url = str(requests.get(
            #   f"https://api.npms.io/v2/package/{pkgname.name}"
            # ).json()["collected"]["metadata"]["links"]["homepage"])
            raise NotImplementedError()
        if pkgtype == PkgType.VSCODE:
            ext = vsc_client.find(name)
            return ext.source, ext.source, ext.latest_manifest.license, ext.versions[0].version
        raise SkipResolver()
