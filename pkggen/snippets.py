from typing import NewType

PkgverFun = NewType('PkgverFun', str)
PrepareFun = NewType('PrepareFun', str)
BuildFun = NewType('BuildFun', str)
CheckFun = NewType('CheckFun', str)
PackageFun = NewType('PackageFun', str)


pkgver_git = PkgverFun("""\
pkgver() {
    cd "${srcdir}/${pkgname}"
    git describe --tags | sed -r 's/^v//;s/([^-]*-g)/r\\1/;s/-/./g'
}
""")

prepare_latest_tag = PrepareFun("""\
prepare(){
  cd "${srcdir}/${pkgname}"
  git reset --hard $(git describe --tags $(git rev-list --tags --max-count=1))
}
""")

package_node_tgz = PackageFun("""\
package() {
    npm install -g --prefix "${pkgdir}/usr" "${srcdir}/${pkgname}-${pkgver}.tgz"

    # Non-deterministic race in npm gives 777 permissions to random directories.
    # See https://github.com/npm/cli/issues/1103 for details.
    find "${pkgdir}/usr" -type d -exec chmod 755 {} +

    # npm gives ownership of ALL FILES to build user
    # https://bugs.archlinux.org/task/63396
    chown -R root:root "${pkgdir}"
}
""")

build_node_src = BuildFun("""\
build(){
  cd "${srcdir}/${pkgname}"
  npm pack
}
""")

package_node_src = PackageFun("""\
package() {
    # TODO: Add jq or something for the version and do not use globs
    npm install -g --prefix "${pkgdir}/usr" ${srcdir}/${pkgname}/${pkgname}-*.tgz

    # Non-deterministic race in npm gives 777 permissions to random directories.
    # See https://github.com/npm/cli/issues/1103 for details.
    find "${pkgdir}/usr" -type d -exec chmod 755 {} +

    # npm gives ownership of ALL FILES to build user
    # https://bugs.archlinux.org/task/63396
    chown -R root:root "${pkgdir}"
}
""")
