from functools import cached_property
from pydantic import BaseModel
import re

_to_camel = re.compile(r'(?<!^)(?=[A-Z])')
def camel_to_snake(t: str) -> str:
  return _to_camel.sub(r'_', t).lower()

def snake_to_camel(t: str) -> str:
  i = iter(t.split('_'))
  return next(i) + "".join(p.title() for p in i)

class CamelModel(BaseModel):
  class Config:
    alias_generator = snake_to_camel
    keep_untouched = (cached_property,)
