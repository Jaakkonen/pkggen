from typing import Optional
from requests import Session
from urllib.parse import urljoin

class PrefixSession(Session):
  """Requests session with prefix and timeout
  """
  def __init__(self, prefix_url: str = '', timeout: Optional[int] = None, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.prefix_url = prefix_url
    self.timeout = timeout
  def request(self, method, url, *args, **kwargs):
    url = urljoin(self.prefix_url, url)
    return super().request(method, url, *args, timeout=self.timeout, **kwargs)
