"""
Visual studio code gallery client
"""
from . import _client as __client
from ._types import *


def search(name: str) -> list[Extension]:
  return [Extension(**ext) for ext in __client.search(name)]

def find(name: str, publisher: Optional[str] = None) -> Extension:
  if publisher is not None:
    ext = Extension(**__client.get(publisher, name))
  else:
    exts = search(name)
    matching_exts = [ext for ext in exts if ext.extension_name == name]
    if len(matching_exts) > 1:
      raise ValueError(f"Multiple extensions with {name=} found. {matching_exts}")
    elif len(matching_exts) == 0:
      raise ValueError(f"No extension found with {name=}")
    ext = matching_exts[0]
  return ext
