from pkggen.util.prefix_req_session import PrefixSession

base = "https://open-vsx.org/vscode/"
base_ms = "https://marketplace.visualstudio.com/_apis/public/"
s = PrefixSession(base, timeout=5)
s.headers.update(
    {
        #"Accept": "application/json;api-version=3.0-preview.1",

            'Connection': 'keep-alive',
    'X-Market-User-Id': '3003736d-89f8-47c7-b852-996535cf3917',
    'Accept': 'application/json;api-version=3.0-preview.1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) code-oss/1.58.0 Chrome/89.0.4389.128 Electron/12.1.0 Safari/537.36',
    'X-Market-Client-Id': 'VSCode 1.58.0',
    'Content-Type': 'application/json',
    'Sec-Fetch-Site': 'cross-site',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Dest': 'empty',
    'Accept-Language': 'en-US',
    }
)


def _extension_query(filters: dict) -> dict:
    """
    Example filter:
    {
      "filters": [
        {
          "criteria": [
            {"filterType": 8, "value": "Microsoft.VisualStudio.Code"},
            {"filterType": 10, "value": "python"},
            {"filterType": 12, "value": "4096"},
          ],
          "pageNumber": 1,
          "pageSize": 50,
          "sortBy": 0,
          "sortOrder": 0,
        }
      ],
      "assetTypes": [],
      "flags": 950,
    }
    """
    r = s.post("gallery/extensionquery", json=filters)
    assert r.status_code == 200

    return r.json()

def extension_query(criteria: dict[int, str], pagesize: int = 50) -> list[dict]:
  r = _extension_query({
            "filters": [
                {
                    "criteria": [
                        {"filterType": t, "value": v}
                        for t, v in criteria.items()
                    ],
                    "pageNumber": 1,
                    "pageSize": pagesize,
                    "sortBy": 0,
                    "sortOrder": 0,
                }
            ],
            "assetTypes": [],
            "flags": 950,
        }
    )
  return r["results"][0]["extensions"]

def get(publisher: str, name: str) -> dict:
    r = extension_query(
      {
        8: 'Microsoft.VisualStudio.Code',
        7: f'{publisher}.{name}',
        12: "4096"
      },
      pagesize=1
    )
    assert len(r) == 1
    return r[0]


def search(name: str) -> list[dict]:
    return extension_query(
      {
        8: 'Microsoft.VisualStudio.Code',
        10: name,
        12: "4096"
      }
    )


def get_manifest(publisher: str, name: str, version: str) -> dict:
  r = s.get(f"asset/{publisher}/{name}/{version}/Microsoft.VisualStudio.Code.Manifest")
  assert r.status_code == 200
  return r.json()
