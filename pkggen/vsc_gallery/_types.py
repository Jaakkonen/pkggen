# There's some manifest thing describing the data type at
# https://github.com/microsoft/vscode-vsce/blob/main/resources/extension.vsixmanifest
from datetime import datetime
from functools import cached_property
from typing import NewType, Optional
from uuid import UUID

from pkggen.util.camel_model import CamelModel
from pydantic.main import BaseModel
from pydantic.networks import HttpUrl

from ._client import get_manifest


class ExtensionManifestRepository(BaseModel):
    type: str
    url: HttpUrl


SPDXLicense = NewType("SPDXLicense", str)


class ExtensionManifest(CamelModel):
    name: str
    version: str
    engines: dict[str, str]  # `vscode` key should be here
    repository: ExtensionManifestRepository
    description: str
    license: SPDXLicense
    publisher: str
    categories: list[str]
    keywords: list[str]
    icon: str
    # galleryBanner:
    # contributes
    # devDependencies


class Publisher(CamelModel):
    publisher_id: UUID
    publisher_name: str
    display_name: Optional[str]


class ExtensionFile(CamelModel):
    asset_type: str  # Actually Literal. See extension.vsixmanifest for details
    source: HttpUrl


class ExtensionVersionProperty(BaseModel):
    key: str
    value: str


class ExtensionVersion(CamelModel):
    version: str
    last_updated: datetime
    files: list[ExtensionFile]
    properties: list[ExtensionVersionProperty]
    asset_uri: HttpUrl
    fallback_asset_uri: HttpUrl

    @property
    def property_dict(self) -> dict[str, str]:
        return {p.key: p.value for p in self.properties}


class Extension(CamelModel):
    """Return type of extension query"""

    publisher: Publisher
    extension_id: UUID
    extension_name: str
    display_name: Optional[str]
    flags: str
    shortDescription: str
    versions: list[ExtensionVersion]

    @cached_property
    def latest_manifest(self):
        # TODO: Cache
        manifest = ExtensionManifest(
            **get_manifest(
                self.publisher.publisher_name,
                self.extension_name,
                self.versions[0].version,
            )
        )
        return manifest

    @cached_property
    def source(self):
        src = self.versions[0].property_dict.get(
            "Microsoft.VisualStudio.Services.Links.Source"
        )
        if src:
            return src
        return self.source
