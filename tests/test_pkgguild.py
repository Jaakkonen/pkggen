
from pkggen.pkgbuild import Maintainer, Pkgbuild
from pkggen.snippets import PackageFun


def test_pkgbuild():
    assert (
        str(
            Pkgbuild(
                pkgname="test_package",
                depends=("systemd",),
                maintainers=(Maintainer("Shrek", "shrek@example.org"),),
                package=PackageFun("package(){ echo nut >> $pkgdir/nut }\n")
            )
        )
        == """\
# Maintainer: Shrek <shrek@example.org>

pkgname=test_package
pkgver=0
pkgrel=1

arch=(any)
url=None
depends=(
  'systemd'
)


package(){ echo nut >> $pkgdir/nut }
"""
    )


def test_pkgbuild_depends():
    assert (
        str(
            Pkgbuild(
                pkgname="test_package",
                depends=("systemd",),
                optdepends=("systemd-libs",),
                maintainers=(Maintainer("Shrek", "shrek@example.org"),),
                package=PackageFun("package(){ echo nut >> $pkgdir/nut }\n")
            )
        )
        == """\
# Maintainer: Shrek <shrek@example.org>

pkgname=test_package
pkgver=0
pkgrel=1

arch=(any)
url=None
depends=(
  'systemd'
)
optdepends=(
  'systemd-libs'
)


package(){ echo nut >> $pkgdir/nut }
"""
    )
