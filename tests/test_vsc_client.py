from uuid import UUID
import pkggen.vsc_gallery as vsc_client
import pkggen.vsc_gallery._client as priv_vsc_client

def test_extension_query():
    exts = priv_vsc_client._extension_query(
        {
            "filters": [
                {
                    "criteria": [
                        {"filterType": 8, "value": "Microsoft.VisualStudio.Code"},
                        {"filterType": 10, "value": "python"},
                        {"filterType": 12, "value": "4096"},
                    ],
                    "pageNumber": 1,
                    "pageSize": 50,
                    "sortBy": 0,
                    "sortOrder": 0,
                }
            ],
            "assetTypes": [],
            "flags": 950,
        }
    )
    assert isinstance(exts, dict)
    assert len(exts['results'][0]['extensions']) > 30 # 33

def test_search():
    exts = vsc_client.search('python')
    for ext in exts:
        assert isinstance(ext, vsc_client.Extension)
    assert len(exts) > 30 # 33

def test_latest_manifest():
    manifest = vsc_client.search('python')[0].latest_manifest
    assert isinstance(manifest, vsc_client.ExtensionManifest)

def test_find_exact():
    ext = vsc_client.find(name='python', publisher='ms-python')
    assert ext.extension_id == UUID('f1f59ae4-9318-4f3c-a9b5-81b2eaa5f8a5')
    assert ext.display_name == 'Python'

def test_find_only_name():
    ext = vsc_client.find(name='python')
    assert ext.extension_id == UUID('f1f59ae4-9318-4f3c-a9b5-81b2eaa5f8a5')
    assert ext.display_name == 'Python'
